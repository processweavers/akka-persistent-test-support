package net.processweavers.testsupport

import akka.actor.{ ActorIdentity, Identify, PoisonPill }
import akka.testkit.{ TestKitBase, TestProbe }

trait ActorTerminator { this: TestKitBase =>

  def gracefullyTerminateActor(path: String): Unit = {
    system.log.info(s"=> Gracefully shutdown actor $path")

    val probe = TestProbe()
    system.actorSelection(path).tell(Identify(1), probe.ref)
    val actorRef = probe.expectMsgPF(hint = "identify response") {
      case ActorIdentity(1, Some(ref)) => ref
      case _ =>
        system.log.warning("No actor with path={} found!", path)
        return
    }
    probe.watch(actorRef)
    actorRef.tell(PoisonPill, probe.ref)
    probe.expectTerminated(actorRef)
    system.log.info("=> ... terminated, waiting grace period ....")
    Thread.sleep(500)
    system.log.info("=> ... finally terminated!")
  }

  def gracefullyTerminatUserActor(actorName: String) = gracefullyTerminateActor(s"/user/$actorName")

}
