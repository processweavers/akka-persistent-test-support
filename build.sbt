import org.scalastyle.sbt.ScalastylePlugin

enablePlugins(SbtScalariform)
enablePlugins(SbtLicenseReport)
enablePlugins(ScalastylePlugin)

lazy val akkaHttpVersion = "10.0.11"
lazy val akkaVersion    = "2.5.7"

// Seems to be an ugly work around for obtaining a task where compile can depend on ...
lazy val compileScalastyle = taskKey[Unit]("compileScalastyle")
compileScalastyle := scalastyle.in(Compile).toTask("").value

lazy val releaseSettings = Seq(
  publishMavenStyle := true,
  pomIncludeRepository := { _ => false },
  publishArtifact in Test := false,
  publishTo := {
    val repo = "https://oss.sonatype.org/"
    if (isSnapshot.value)
      Some("snapshots" at repo + "content/repositories/snapshots")
    else
      Some("releases" at repo + "service/local/staging/deploy/maven2")
  },
  pomExtra :=
    <url>https://bitbucket.org/processweavers/rbpl/wiki/Home</url>
    <scm>
      <connection>scm:git:git://bitbucket.org/bitbucket.org/processweavers/rbpl-example.git</connection>
      <developerConnection>scm:git:ssh://bitbucket.org:processweavers/rbpl-example.git</developerConnection>
      <url>https://bitbucket.org/processweavers/rbpl-example/src</url>
    </scm>
    <licenses>
      <license>
        <name>BSD-3-Clause</name>
        <url>https://opensource.org/licenses/BSD-3-Clause</url>
        <distribution>repo</distribution>
      </license>
    </licenses>
    <developers>
      <developer>
        <name>Carsten Seibert</name>
        <email>seibert@seibertec.ch</email>
        <organization>seiberTEC GmbH</organization>
        <organizationUrl>http://www.seibertec.ch</organizationUrl>
      </developer>
      <developer>
        <name>Anke Seibert-Otto</name>
        <email>anke.seibert@seibertec.ch</email>
        <organization>seiberTEC GmbH</organization>
        <organizationUrl>http://www.seibertec.ch</organizationUrl>
      </developer>
    </developers>
)

lazy val root = (project in file("."))
  .settings(
      concurrentRestrictions in Global += Tags.limit(Tags.Test, 1),
      inThisBuild(List(
        scalaVersion    := "2.12.4",
        scalacOptions ++= Seq(
          "-unchecked",
          "-deprecation",
          "-Xexperimental",
          "-feature",
          "-language:implicitConversions",
          "-language:reflectiveCalls"
        )
      )),
    name            := "akka-persistent-test-support",
    organization    := "net.processweavers",
    version         := "0.0.1",
    (compile in Compile) := ((compile in Compile) dependsOn compileScalastyle).value,
    resolvers += Resolver.jcenterRepo,
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor"       % akkaVersion,
      "com.typesafe.akka" %% "akka-persistence" % akkaVersion,
      "com.typesafe.akka" %% "akka-slf4j"       % akkaVersion,
      "com.typesafe.akka" %% "akka-testkit"     % akkaVersion,
      "org.iq80.leveldb"            % "leveldb"          % "0.9",
      "org.fusesource.leveldbjni"   % "leveldbjni-all"   % "1.8",
      "org.scalatest"     %% "scalatest"         % "3.0.1"
    )
  )
  .settings(releaseSettings: _*)
