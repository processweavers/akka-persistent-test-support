package net.processweavers.testsupport

import akka.actor.ActorSystem
import akka.testkit.{ ImplicitSender, TestKit }
import com.typesafe.config.{ Config, ConfigFactory }
import org.scalatest.{ Assertions, BeforeAndAfterAll, Matchers, WordSpecLike }
import org.scalatest.concurrent.{ ScalaFutures, Waiters }

import scala.concurrent.Future
import scala.reflect.io.Path
import scala.util.{ Failure, Success, Try }

abstract class PersistentActorTestKit(actorSystemName: String, config: Config = PersistentActorTestKit.config)
  extends TestKit(ActorSystem(actorSystemName, config)) with WordSpecLike
  with Matchers
  with ScalaFutures
  with BeforeAndAfterAll
  with ActorTerminator
  with ImplicitSender {

  import system.dispatcher

  override protected def beforeAll(): Unit = {
    deleteDBFiles()
  }

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  protected def deleteDBFiles(): Unit = {
    val path: Path = Path(PersistentActorTestKit.Dir)
    Try(path.deleteRecursively())
  }

  object FailingFuture {
    implicit class Failing[A](val f: Future[A]) extends Assertions with Waiters {
      def failing[T <: Throwable](implicit m: Manifest[T]) = {
        val w = new Waiter
        f onComplete {
          case Failure(e) =>
            w(throw e); w.dismiss()
          case Success(_) => w.dismiss()
        }
        intercept[T] {
          w.await
        }
      }
    }
  }
}

object PersistentActorTestKit {
  val Dir = "target/journal"
  lazy val config = ConfigFactory.parseString(
    s"""
       | akka {
       |   persistence {
       |     journal.plugin = "akka.persistence.journal.leveldb"
       |     journal.leveldb {
       |       dir = $Dir
       |       native = off
       |     }
       |   }
       | }
      """.stripMargin).withFallback(ConfigFactory.load()).resolve()
}
